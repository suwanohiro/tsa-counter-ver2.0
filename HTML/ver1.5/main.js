var player_name = [
    "", "", "", "", "", "", "", ""
];

var player_count = [
    0, 0, 0, 0, 0, 0, 0, 0
];

var max_player = "";
var max_count = 0;

var dis = 0;
var dis_mode = [
    "main",
    "main2",
    "history"
];
var sw = screen.width,
    sh = screen.height,
    ww = window.innerWidth,
    wh = window.innerHeight;

var tmp = 0,
    tmp2 = 0;

var dis_num = 0;

var count_mode = "+";

function onloads() {
    setInterval(save, 10);
    setInterval(colors, 10);
    setInterval(keybord, 10);
    setInterval(sizes, 10);
    setInterval(tables, 10);
    setInterval(reader, 10);
    setInterval(max_p, 5);

    var url = String(location.href);
    var key = url.substr(url.indexOf('?') + 1);
    if (key == "display2") displays(1);
}

function sizes() {
    sw = screen.width;
    sh = screen.height;
    ww = window.innerWidth;
    wh = window.innerHeight;
}

function player_names(num, num2) {
    if (num2 == 1) {
        player_name[num - 1] = window.prompt("名前を入力してください", player_name[num - 1]);
        if (player_name[num - 1] == null) player_name[num - 1] = "";
        document.getElementById("No" + num).innerHTML = player_name[num - 1];
    } else {
        player_name[num - 1] = "";
        document.getElementById("No" + num).innerHTML = "";
        player_count[num - 1] = 0;
        document.getElementById("CT" + num).innerHTML = player_count[num - 1];
        max_player = "";
        max_count = 0;
        document.getElementById("read_No.1").innerHTML = max_player;
        document.getElementById("read_CT.1").innerHTML = max_count;
    }
}

function inter() {
    save();
}

function save() {
    localStorage.setItem('No1', player_name[0]);
    localStorage.setItem('No2', player_name[1]);
    localStorage.setItem('No3', player_name[2]);
    localStorage.setItem('No4', player_name[3]);
    localStorage.setItem('No5', player_name[4]);
    localStorage.setItem('No6', player_name[5]);
    localStorage.setItem('No7', player_name[6]);
    localStorage.setItem('No8', player_name[7]);

    localStorage.setItem('CT1', player_count[0]);
    localStorage.setItem('CT2', player_count[1]);
    localStorage.setItem('CT3', player_count[2]);
    localStorage.setItem('CT4', player_count[3]);
    localStorage.setItem('CT5', player_count[4]);
    localStorage.setItem('CT6', player_count[5]);
    localStorage.setItem('CT7', player_count[6]);
    localStorage.setItem('CT8', player_count[7]);
}

function test() {
    alert(player_name[0]);
    localStorage.setItem('No1', player_name[0]);
}

function count(num, num2) {
    if (player_name[num - 1] != "") {

        player_count[num - 1] += num2;

        if (num2 == 0) player_count[num - 1] = 0;

        if (player_count[num - 1] < 0) player_count[num - 1] = 0;

        document.getElementById("CT" + num).innerHTML = player_count[num - 1];
    }

    if (num2 <= 0) max_count = 0;
}

function all_count(num, num2) {
    var asd = 1;

    while (asd <= 8) {
        if (player_name[asd - 1] != "") {
            if (num == "+") all_counts(asd, num2);
            else all_counts(asd, (num2 * -1));
        }
        asd++;
    }

    if (num2 <= 0) max_count = 0;
}

function all_counts(num, num2) {
    //alert("num : " + num + "\nnum2 : " + num2);
    player_count[num - 1] += num2;
    if (num2 == 0) player_count[num - 1] = 0;
    if (player_count[num - 1] < 0) player_count[num - 1] = 0;
    document.getElementById("CT" + num).innerHTML = player_count[num - 1];

    if (num2 <= 0) max_count = 0;
}

function colors() {
    var asd = 0,
        color,
        txt_color;

    while (asd < 8) {
        if (player_count[asd] != max_count) {
            if (player_name[asd] != "" && player_count[asd] != max_count) {
                color = "#00008b";
                txt_color = "#FFFFFF";
            } else {
                color = "#87ceeb";
                txt_color = "#000000";
            }
            document.getElementById(asd + 1).style.backgroundColor = color;
            document.getElementById(asd + 1).style.color = txt_color;
        } else {
            if (player_name[asd] != "") {
                document.getElementById(asd + 1).style.backgroundColor = "#000000";
                document.getElementById(asd + 1).style.color = "#FFFFFF";
            }
        }
        asd++;
    }
}

var old = 0;

function displays(num) {
    /*dis++;
    if (dis % 2 == 1) {
        document.getElementById("history").style.display = "block";
        document.getElementById("main").style.display = "none";
    } else {
        document.getElementById("history").style.display = "none";
        document.getElementById("main").style.display = "block";
    }*/

    if (old == 0 && num == 1) dis_num = 1;
    if (old == 1 && num == 0) dis_num = 0;

    document.getElementById("main").style.display = "none";
    document.getElementById("main2").style.display = "none";
    document.getElementById("history").style.display = "none";

    document.getElementById(dis_mode[num]).style.display = "block";

    old = num;
}

function keybord() {
    //alert("a");
    addEventListener("keydown", keydownfunc, false);
}

function keydownfunc(event) {
    var key_code = event.keyCode,
        counter = 1,
        a = 1,
        b = 0;

    if (count_mode == "+") counter = 1;
    if (count_mode == "-") counter = 1;

    if (key_code >= 96 && key_code <= 105) key_code -= 48;
    if (key_code == 107) key_code = 38;
    if (key_code == 109) key_code = 40;
    if (key_code >= 49 && key_code <= 56) {
        switch (count_mode) {
            case "+":
                count((key_code - 48), 1);
                break;

            case "-":
                count((key_code - 48), -1);
                break;

            case "*":
                player_names((key_code - 48), 1);
                key_code = 38;
                break;

            case "Del":
                player_names((key_code - 48), 0);
                key_code = 38;
                break;

            default:
                alert("keydownfunc : エラー\ncount_mode : " + count_mode);
                break;
        }
    }

    switch (key_code) {
        case 13:
            dis_num++;
            displays(dis_num % 2);
            break;

        case 16: //Shift
            count_mode = "*";
            document.title = "TSA Counter" + "　【↓】";
            document.getElementById("mode").innerHTML = "↓";
            break;

        case 37:
            all_count('-', 1);
            break;

        case 38: // + or ↑
            count_mode = "+";
            document.title = "TSA Counter" + "　【＋】";
            document.getElementById("mode").innerHTML = "＋";
            break;

        case 39:
            all_count('+', 1);
            break;

        case 40: // - or ↓
            count_mode = "-";
            document.title = "TSA Counter" + "　【－】";
            document.getElementById("mode").innerHTML = "－";
            break;

        case 46: //Delete
            count_mode = "Del";
            document.title = "TSA Counter" + "　【Del】";
            document.getElementById("mode").innerHTML = "Del";
            break;

        case 48: //0
            while (a <= 8) {
                while (player_count[a - 1] > 0) {
                    count(a, -1);
                }
                a++;
            }
            a = 1;
            break;

        case 49: //1
            break;

        case 50: //2
            break;

        case 50: //3
            break;

        case 51: //4
            break;

        case 52: //5
            break;

        case 53: //6
            break;

        case 54: //7
            break;

        case 55: //8
            break;

        case 56: //9
            break;

        default:
            //alert(key_code);
            break;
    }
}


function num_button(num) {
    var a = 1;
    if (num == 0) {
        while (a <= 8) {
            while (player_count[a - 1] > 0) {
                count(a, -1);
            }
            a++;
        }
    }

    if (num >= 1 && num <= 8) {
        switch (count_mode) {
            case "+":
                count(num, 1);
                break;

            case "-":
                count(num, -1);
                break;

            case "*":
                player_names(num, 1);
                count_mode = "+";
                document.title = "TSA Counter" + "　【＋】";
                document.getElementById("mode").innerHTML = "＋";
                break;

            case "Del":
                player_names(num, 0);
                count_mode = "+";
                document.title = "TSA Counter" + "　【＋】";
                document.getElementById("mode").innerHTML = "＋";
                break;

            default:
                alert("keydownfunc : エラー\ncount_mode : " + count_mode);
                break;
        }
    }

    if (num >= 10 && 13) {
        switch (num) {
            case 10: //+
                count_mode = "+";
                document.title = "TSA Counter" + "　【＋】";
                document.getElementById("mode").innerHTML = "＋";
                break;

            case 11: //-
                count_mode = "-";
                document.title = "TSA Counter" + "　【－】";
                document.getElementById("mode").innerHTML = "－";
                break;

            case 12: //Del
                count_mode = "Del";
                document.title = "TSA Counter" + "　【Del】";
                document.getElementById("mode").innerHTML = "Del";
                break;

            case 13: //Shift
                count_mode = "*";
                document.title = "TSA Counter" + "　【↓】";
                document.getElementById("mode").innerHTML = "↓";
                break;
        }
    }
}

function tables() {
    document.getElementById("num_key").style.height = wh - 100 + "px";
    document.getElementById("read").style.height = wh - 100 + "px";
}


function reader() {
    document.getElementById("read_No1").innerHTML = player_name[0];
    document.getElementById("read_No2").innerHTML = player_name[1];
    document.getElementById("read_No3").innerHTML = player_name[2];
    document.getElementById("read_No4").innerHTML = player_name[3];
    document.getElementById("read_No5").innerHTML = player_name[4];
    document.getElementById("read_No6").innerHTML = player_name[5];
    document.getElementById("read_No7").innerHTML = player_name[6];
    document.getElementById("read_No8").innerHTML = player_name[7];

    document.getElementById("read_CT1").innerHTML = player_count[0];
    document.getElementById("read_CT2").innerHTML = player_count[1];
    document.getElementById("read_CT3").innerHTML = player_count[2];
    document.getElementById("read_CT4").innerHTML = player_count[3];
    document.getElementById("read_CT5").innerHTML = player_count[4];
    document.getElementById("read_CT6").innerHTML = player_count[5];
    document.getElementById("read_CT7").innerHTML = player_count[6];
    document.getElementById("read_CT8").innerHTML = player_count[7];
}


function max_p() {
    tmp2 = 0;
    for (tmp = 0; tmp < 8; tmp++) {
        if ((player_count[tmp] > max_count) && (player_name[tmp] != "")) max_count = player_count[tmp];
    }

    for (tmp = 0; tmp < 8; tmp++) {
        if ((player_count[tmp] == max_count) && (tmp2 == 1) && (player_name[tmp] != "")) {
            max_player = max_player + "<br>" + "【" + (tmp + 1) + "P" + "】" + player_name[tmp];
            document.getElementById(tmp + 1).style.backgroundColor = "#000000";
            document.getElementById(tmp + 1).style.color = "#FFFFFF";
        }
        if ((player_count[tmp] == max_count) && (tmp2 == 0) && (player_name[tmp] != "")) {
            max_player = "【" + (tmp + 1) + "P" + "】" + player_name[tmp];
            document.getElementById(tmp + 1).style.backgroundColor = "#000000";
            document.getElementById(tmp + 1).style.color = "#FFFFFF";
            tmp2 = 1;
        }
    }

    document.getElementById("read_No.1").innerHTML = max_player;
    document.getElementById("read_CT.1").innerHTML = max_count;
}

var max_dis_num = 0;

function max_dis() {

    document.getElementById("max").style.display = "none";
    document.getElementById("players").style.display = "none";

    max_dis_num++;
    if (max_dis_num % 2 == 1) {
        document.getElementById("max").style.display = "block";
        document.getElementById("max").style.width = "100%";
    } else {
        document.getElementById("players").style.display = "block";
        document.getElementById("players").style.width = "100%";
    }
}