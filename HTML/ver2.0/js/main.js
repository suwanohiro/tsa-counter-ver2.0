window.onload = function () {
    表作成();
    setInterval(write, 10);
    setInterval(画面構成, 10);
}

function 画面構成() {
    var ww = window.innerWidth;
    var wh = window.innerHeight;
    var main = document.getElementById("main");
    if (ww > 1000) {
        main.style.paddingLeft = "20vw";
        main.style.width = "60%";
    } else {
        main.style.paddingLeft = "0vw";
        main.style.width = "100%";
    }
}

function 表作成() {
    var player_count = document.getElementById("player_count");
    var table = document.createElement("table");
    table.id = "player_count_table";
    player_count.appendChild(table);
    var pct = document.getElementById("player_count_table");
    for (var cnt1 = 0; cnt1 < 8; cnt1++) {
        var tr = document.createElement("tr");
        tr.id = `tr_cnt1_${cnt1}`;
        pct.appendChild(tr);
        for (var cnt2 = 0; cnt2 < 3; cnt2++) {
            var tr_id = document.getElementById(`tr_cnt1_${cnt1}`);
            var td = document.createElement("td");
            td.className = `td_cnt2_${cnt2}`;
            if (cnt2 == 0) td.innerHTML = `${cnt1 + 1}P`;
            if (cnt2 == 2) td.innerHTML = "0回";
            tr_id.appendChild(td);
        }
    }
}

function write() {
    var data = text.csv_read("csv/data.csv");
    var player_number = [];
    var player_name = [];
    var player_count = [];

    /*
    var debug = [0, 1, 0, 1, 1, 1, 1, 0];
    for (var a = 0; a < debug.length; a++) {
        data = cnt_plus(data, (a + 1), debug[a]);
    }
    data[8][1] = 2; //計測開始回数
    data[8][2] = 3; //対戦数
    */

    for (var a = 0; a < data.length; a++) {
        player_number[a] = data[a][0];
        player_name[a] = data[a][1];
        player_count[a] = data[a][2];
    }
    player_name_write(player_name);
    player_count_write(player_count);
    max_players(player_number, player_name, player_count);
}

function cnt_plus(data, player, cnt) {
    data[player - 1][2] = cnt;
    return data;
}

function player_name_write(player_name) {
    var td_cnt2_1 = document.getElementsByClassName("td_cnt2_1");
    for (var a = 0; a < 8; a++) {
        td_cnt2_1[a].innerHTML = player_name[a];
    }
}

function player_count_write(player_count) {
    var td_cnt2_2 = document.getElementsByClassName("td_cnt2_2");
    for (var a = 0; a < 8; a++) {
        td_cnt2_2[a].innerHTML = `${player_count[a]}回`;
    }
}

function max_players(player_number, player_name, player_count) {
    var battle_count = document.getElementById("battle_count");
    var count_start = document.getElementById("count_start");
    battle_count.innerHTML = player_count[8];
    count_start.innerHTML = player_name[8];

    player_count.splice(8, (player_count.length - 1));
    for (var a = 0; a < player_count.length; a++) {
        player_count[a] = Number(player_count[a]);
    }

    var max_count = document.getElementById("max_count");
    var max = Excel.max(player_count);
    max_count.innerHTML = max;

    var max_player = document.getElementById("max_player");
    var str = "";
    var cnt = 0;
    for (var a = 0; a < 8; a++) {
        if (player_count[a] == max) {
            if (cnt != 0) str += "<br>";
            str += `【${a}P】${player_name[a]}&nbsp;さん`;
            cnt++;
        }
    }
    str += `<br><br><div style='text-align: right; width: 100%; font-size: 1vw;'>以上 <span style='font-size: 5vw; font-weight: bold; color: red;'>${cnt}名</span> の内どなたか交代のお願いの際には交代をお願いします。</div>`;
    if (player_name[8] < battle_count.innerHTML) max_player.innerHTML = str;
    else max_player.innerHTML = `計測開始まで残り${player_name[8] - battle_count.innerHTML + 1}回`;
}