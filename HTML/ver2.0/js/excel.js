class Excel {
    static average(data) {
        var sum = 0;
        var after = this.numerical_extraction(data);
        for (var a = 0; a < after.length; a++) sum += Number(after[a]);
        return Number(sum / after.length);
    }

    static max(data) {
        var after = this.numerical_extraction(data);
        var Max = after[0];
        for (var a = 1; a < after.length; a++) {
            if (Max < after[a]) Max = after[a];
        }
        return Number(Max);
    }

    static min(data) {
        var after = this.numerical_extraction(data);
        var Min = after[0];
        for (var a = 1; a < after.length; a++) {
            if (Min > after[a]) Min = after[a];
        }
        return Number(Min);
    }

    static count(data) {
        var cnt = 0;
        for (var a = 0; a < data.length; a++) {
            if (!isNaN(data[a])) cnt++;
        }
        return cnt;
    }

    static count_not(data) {
        var num_count = this.count(data);
        return Number(data.length - num_count);
    }

    static today() {
        var date = new Date();
        return date.toLocaleDateString();
    }

    static now() {
        var date = new Date();
        var td = this.today();
        var day = date.toLocaleTimeString();
        return `${td} ${day.slice(0, 5)}`;
    }

    static numerical_extraction(data) {
        var after = [];
        for (var a = 0; a < data.length; a++) {
            if (!isNaN(data[a])) after[after.length] = data[a];
        }
        return after;
    }

    static string_extraction(data) {
        var after = [];
        for (var a = 0; a < data.length; a++) {
            if (isNaN(data[a])) after[after.length] = data[a];
        }
        return after;
    }
}