class text {
    static normal_read(url) {
        var txt = new XMLHttpRequest();
        txt.open('get', url, false);
        txt.send();
        return txt.responseText;
    }

    static array_read(url) {
        var txt = new XMLHttpRequest();
        txt.open('get', url, false);
        txt.send();
        return txt.responseText.split(/\r\n|\n/);
    }

    static csv_read(url) {
        var txt = new XMLHttpRequest();
        txt.open('get', url, false);
        txt.send();
        var arr = txt.responseText.split(/\r\n|\n/);
        var res = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == '') break;
            res[i] = arr[i].split(',');
        }
        return res;
    }
}