const SpreadSheetService = require('./spreadSheetService')
// 認証情報jsonファイルを読み込む
const CREDIT = require('tsa-counter-0081de3c44cd.json')
// スプレッドシートキー
const SPREADSHEET_KEY = '1BnQv6qVeFTgyetxLVtFT1i6wt3NfL6UW1vU5DjiknaU'

// データを4件追加
const insertMany = async () => {
    await spreadSheetService.insert({
        id: 1,
        name: 'John Doe',
        age: 40
    })
    await spreadSheetService.insert({
        id: 2,
        name: 'Jane Doe',
        age: 30
    })
    await spreadSheetService.insert({
        id: 3,
        name: '山田太郎',
        age: 20
    })
    await spreadSheetService.insert({
        id: 4,
        name: '山田花子',
        age: 30
    })
}

insertMany()
