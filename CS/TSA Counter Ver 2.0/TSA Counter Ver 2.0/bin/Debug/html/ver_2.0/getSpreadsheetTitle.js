const {
    GoogleSpreadsheet
} = require('google-spreadsheet');
// 認証情報jsonファイルを読み込む
const CREDIT = require('tsa-counter-0081de3c44cd.json');
// スプレッドシートキー
const SPREADSHEET_KEY = '1BnQv6qVeFTgyetxLVtFT1i6wt3NfL6UW1vU5DjiknaU';

const getSpreadsheetTitleByKey = async (spreasheetKey) => {
    // 一般ユーザーに公開していないスプレッドシートへアクセスしたい場合, 作成したサービスアカウントに対し
    // 閲覧権限を与えておく.
    const doc = new GoogleSpreadsheet(spreasheetKey);

    // サービスアカウントによる認証
    await doc.useServiceAccountAuth({
        client_email: CREDIT.client_email,
        private_key: CREDIT.private_key,
    });

    // スプレッドシートの情報を読み込みを行い, タイトルを取得
    await doc.loadInfo();
    const sheet = doc.sheetsByIndex[sheetIndex]

    // ヘッダー行を作成する
    await sheet.setHeaderRow(headerValues)
}

getSpreadsheetTitleByKey(SPREADSHEET_KEY, 0, ['id', 'name', 'age'])
// <スプレッドシートのタイトル>
