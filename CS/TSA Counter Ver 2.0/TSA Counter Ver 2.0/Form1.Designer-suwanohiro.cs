﻿namespace TSA_Counter_Ver_2._0
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Player_1_Name = new System.Windows.Forms.TextBox();
            this.Player_1_Reset = new System.Windows.Forms.Button();
            this.Player_1_Save = new System.Windows.Forms.Button();
            this.Player_1_Count = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Player_2_Name = new System.Windows.Forms.TextBox();
            this.Player_2_Count = new System.Windows.Forms.Label();
            this.Player_2_Reset = new System.Windows.Forms.Button();
            this.Player_2_Save = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Player_3_Name = new System.Windows.Forms.TextBox();
            this.Player_3_Count = new System.Windows.Forms.Label();
            this.Player_3_Reset = new System.Windows.Forms.Button();
            this.Player_3_Save = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Player_4_Name = new System.Windows.Forms.TextBox();
            this.Player_4_Count = new System.Windows.Forms.Label();
            this.Player_4_Reset = new System.Windows.Forms.Button();
            this.Player_4_Save = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Player_5_Name = new System.Windows.Forms.TextBox();
            this.Player_5_Count = new System.Windows.Forms.Label();
            this.Player_5_Reset = new System.Windows.Forms.Button();
            this.Player_5_Save = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Player_6_Name = new System.Windows.Forms.TextBox();
            this.Player_6_Count = new System.Windows.Forms.Label();
            this.Player_6_Reset = new System.Windows.Forms.Button();
            this.Player_6_Save = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.Player_7_Name = new System.Windows.Forms.TextBox();
            this.Player_7_Count = new System.Windows.Forms.Label();
            this.Player_7_Reset = new System.Windows.Forms.Button();
            this.Player_7_Save = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.Player_8_Name = new System.Windows.Forms.TextBox();
            this.Player_8_Count = new System.Windows.Forms.Label();
            this.Player_8_Reset = new System.Windows.Forms.Button();
            this.Player_8_Save = new System.Windows.Forms.Button();
            this.Count_All_Reset = new System.Windows.Forms.Button();
            this.Battle_Player = new System.Windows.Forms.TextBox();
            this.Battle_Player_Set = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkedListBox2 = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Chat_Link = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ファイルToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.csvtxt出力ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.csv出力ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txt出力ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.データロードToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.終了ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.フルスクリーンToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.webView21 = new Microsoft.Web.WebView2.WinForms.WebView2();
            this.MaxPlayer = new System.Windows.Forms.Label();
            this.maxplayer_name = new System.Windows.Forms.TextBox();
            this.webView22 = new Microsoft.Web.WebView2.WinForms.WebView2();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.outputFolder = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.account = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.webView21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.webView22)).BeginInit();
            this.SuspendLayout();
            // 
            // Player_1_Name
            // 
            this.Player_1_Name.Location = new System.Drawing.Point(6, 18);
            this.Player_1_Name.Name = "Player_1_Name";
            this.Player_1_Name.Size = new System.Drawing.Size(156, 19);
            this.Player_1_Name.TabIndex = 1;
            this.Player_1_Name.TextChanged += new System.EventHandler(this.Player_1_Name_TextChanged);
            // 
            // Player_1_Reset
            // 
            this.Player_1_Reset.Location = new System.Drawing.Point(6, 66);
            this.Player_1_Reset.Name = "Player_1_Reset";
            this.Player_1_Reset.Size = new System.Drawing.Size(75, 23);
            this.Player_1_Reset.TabIndex = 2;
            this.Player_1_Reset.Text = "リセット";
            this.Player_1_Reset.UseVisualStyleBackColor = true;
            this.Player_1_Reset.Click += new System.EventHandler(this.Player_1_Reset_Click);
            // 
            // Player_1_Save
            // 
            this.Player_1_Save.Location = new System.Drawing.Point(87, 66);
            this.Player_1_Save.Name = "Player_1_Save";
            this.Player_1_Save.Size = new System.Drawing.Size(75, 23);
            this.Player_1_Save.TabIndex = 3;
            this.Player_1_Save.Text = "保存";
            this.Player_1_Save.UseVisualStyleBackColor = true;
            // 
            // Player_1_Count
            // 
            this.Player_1_Count.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Player_1_Count.Location = new System.Drawing.Point(6, 40);
            this.Player_1_Count.Name = "Player_1_Count";
            this.Player_1_Count.Size = new System.Drawing.Size(156, 23);
            this.Player_1_Count.TabIndex = 4;
            this.Player_1_Count.Text = "0";
            this.Player_1_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Player_1_Name);
            this.groupBox1.Controls.Add(this.Player_1_Count);
            this.groupBox1.Controls.Add(this.Player_1_Reset);
            this.groupBox1.Controls.Add(this.Player_1_Save);
            this.groupBox1.Location = new System.Drawing.Point(14, 229);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(169, 97);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "1P";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Player_2_Name);
            this.groupBox2.Controls.Add(this.Player_2_Count);
            this.groupBox2.Controls.Add(this.Player_2_Reset);
            this.groupBox2.Controls.Add(this.Player_2_Save);
            this.groupBox2.Location = new System.Drawing.Point(189, 229);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(169, 97);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "2P";
            // 
            // Player_2_Name
            // 
            this.Player_2_Name.Location = new System.Drawing.Point(6, 18);
            this.Player_2_Name.Name = "Player_2_Name";
            this.Player_2_Name.Size = new System.Drawing.Size(156, 19);
            this.Player_2_Name.TabIndex = 1;
            this.Player_2_Name.TextChanged += new System.EventHandler(this.Player_2_Name_TextChanged);
            // 
            // Player_2_Count
            // 
            this.Player_2_Count.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Player_2_Count.Location = new System.Drawing.Point(6, 40);
            this.Player_2_Count.Name = "Player_2_Count";
            this.Player_2_Count.Size = new System.Drawing.Size(156, 23);
            this.Player_2_Count.TabIndex = 4;
            this.Player_2_Count.Text = "0";
            this.Player_2_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Player_2_Reset
            // 
            this.Player_2_Reset.Location = new System.Drawing.Point(6, 66);
            this.Player_2_Reset.Name = "Player_2_Reset";
            this.Player_2_Reset.Size = new System.Drawing.Size(75, 23);
            this.Player_2_Reset.TabIndex = 2;
            this.Player_2_Reset.Text = "リセット";
            this.Player_2_Reset.UseVisualStyleBackColor = true;
            this.Player_2_Reset.Click += new System.EventHandler(this.Player_2_Reset_Click);
            // 
            // Player_2_Save
            // 
            this.Player_2_Save.Location = new System.Drawing.Point(87, 66);
            this.Player_2_Save.Name = "Player_2_Save";
            this.Player_2_Save.Size = new System.Drawing.Size(75, 23);
            this.Player_2_Save.TabIndex = 3;
            this.Player_2_Save.Text = "保存";
            this.Player_2_Save.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Player_3_Name);
            this.groupBox3.Controls.Add(this.Player_3_Count);
            this.groupBox3.Controls.Add(this.Player_3_Reset);
            this.groupBox3.Controls.Add(this.Player_3_Save);
            this.groupBox3.Location = new System.Drawing.Point(364, 229);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(169, 97);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "3P";
            // 
            // Player_3_Name
            // 
            this.Player_3_Name.Location = new System.Drawing.Point(6, 18);
            this.Player_3_Name.Name = "Player_3_Name";
            this.Player_3_Name.Size = new System.Drawing.Size(156, 19);
            this.Player_3_Name.TabIndex = 1;
            this.Player_3_Name.TextChanged += new System.EventHandler(this.Player_3_Name_TextChanged);
            // 
            // Player_3_Count
            // 
            this.Player_3_Count.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Player_3_Count.Location = new System.Drawing.Point(6, 40);
            this.Player_3_Count.Name = "Player_3_Count";
            this.Player_3_Count.Size = new System.Drawing.Size(156, 23);
            this.Player_3_Count.TabIndex = 4;
            this.Player_3_Count.Text = "0";
            this.Player_3_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Player_3_Reset
            // 
            this.Player_3_Reset.Location = new System.Drawing.Point(6, 66);
            this.Player_3_Reset.Name = "Player_3_Reset";
            this.Player_3_Reset.Size = new System.Drawing.Size(75, 23);
            this.Player_3_Reset.TabIndex = 2;
            this.Player_3_Reset.Text = "リセット";
            this.Player_3_Reset.UseVisualStyleBackColor = true;
            this.Player_3_Reset.Click += new System.EventHandler(this.Player_3_Reset_Click);
            // 
            // Player_3_Save
            // 
            this.Player_3_Save.Location = new System.Drawing.Point(87, 66);
            this.Player_3_Save.Name = "Player_3_Save";
            this.Player_3_Save.Size = new System.Drawing.Size(75, 23);
            this.Player_3_Save.TabIndex = 3;
            this.Player_3_Save.Text = "保存";
            this.Player_3_Save.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Player_4_Name);
            this.groupBox4.Controls.Add(this.Player_4_Count);
            this.groupBox4.Controls.Add(this.Player_4_Reset);
            this.groupBox4.Controls.Add(this.Player_4_Save);
            this.groupBox4.Location = new System.Drawing.Point(539, 229);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(169, 97);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "4P";
            // 
            // Player_4_Name
            // 
            this.Player_4_Name.Location = new System.Drawing.Point(6, 18);
            this.Player_4_Name.Name = "Player_4_Name";
            this.Player_4_Name.Size = new System.Drawing.Size(156, 19);
            this.Player_4_Name.TabIndex = 1;
            this.Player_4_Name.TextChanged += new System.EventHandler(this.Player_4_Name_TextChanged);
            // 
            // Player_4_Count
            // 
            this.Player_4_Count.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Player_4_Count.Location = new System.Drawing.Point(6, 40);
            this.Player_4_Count.Name = "Player_4_Count";
            this.Player_4_Count.Size = new System.Drawing.Size(156, 23);
            this.Player_4_Count.TabIndex = 4;
            this.Player_4_Count.Text = "0";
            this.Player_4_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Player_4_Reset
            // 
            this.Player_4_Reset.Location = new System.Drawing.Point(6, 66);
            this.Player_4_Reset.Name = "Player_4_Reset";
            this.Player_4_Reset.Size = new System.Drawing.Size(75, 23);
            this.Player_4_Reset.TabIndex = 2;
            this.Player_4_Reset.Text = "リセット";
            this.Player_4_Reset.UseVisualStyleBackColor = true;
            this.Player_4_Reset.Click += new System.EventHandler(this.Player_4_Reset_Click);
            // 
            // Player_4_Save
            // 
            this.Player_4_Save.Location = new System.Drawing.Point(87, 66);
            this.Player_4_Save.Name = "Player_4_Save";
            this.Player_4_Save.Size = new System.Drawing.Size(75, 23);
            this.Player_4_Save.TabIndex = 3;
            this.Player_4_Save.Text = "保存";
            this.Player_4_Save.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Player_5_Name);
            this.groupBox5.Controls.Add(this.Player_5_Count);
            this.groupBox5.Controls.Add(this.Player_5_Reset);
            this.groupBox5.Controls.Add(this.Player_5_Save);
            this.groupBox5.Location = new System.Drawing.Point(14, 332);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(169, 97);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "5P";
            // 
            // Player_5_Name
            // 
            this.Player_5_Name.Location = new System.Drawing.Point(6, 18);
            this.Player_5_Name.Name = "Player_5_Name";
            this.Player_5_Name.Size = new System.Drawing.Size(156, 19);
            this.Player_5_Name.TabIndex = 1;
            this.Player_5_Name.TextChanged += new System.EventHandler(this.Player_5_Name_TextChanged);
            // 
            // Player_5_Count
            // 
            this.Player_5_Count.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Player_5_Count.Location = new System.Drawing.Point(6, 40);
            this.Player_5_Count.Name = "Player_5_Count";
            this.Player_5_Count.Size = new System.Drawing.Size(156, 23);
            this.Player_5_Count.TabIndex = 4;
            this.Player_5_Count.Text = "0";
            this.Player_5_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Player_5_Reset
            // 
            this.Player_5_Reset.Location = new System.Drawing.Point(6, 66);
            this.Player_5_Reset.Name = "Player_5_Reset";
            this.Player_5_Reset.Size = new System.Drawing.Size(75, 23);
            this.Player_5_Reset.TabIndex = 2;
            this.Player_5_Reset.Text = "リセット";
            this.Player_5_Reset.UseVisualStyleBackColor = true;
            this.Player_5_Reset.Click += new System.EventHandler(this.Player_5_Reset_Click);
            // 
            // Player_5_Save
            // 
            this.Player_5_Save.Location = new System.Drawing.Point(87, 66);
            this.Player_5_Save.Name = "Player_5_Save";
            this.Player_5_Save.Size = new System.Drawing.Size(75, 23);
            this.Player_5_Save.TabIndex = 3;
            this.Player_5_Save.Text = "保存";
            this.Player_5_Save.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Player_6_Name);
            this.groupBox6.Controls.Add(this.Player_6_Count);
            this.groupBox6.Controls.Add(this.Player_6_Reset);
            this.groupBox6.Controls.Add(this.Player_6_Save);
            this.groupBox6.Location = new System.Drawing.Point(189, 332);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(169, 97);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "6P";
            // 
            // Player_6_Name
            // 
            this.Player_6_Name.Location = new System.Drawing.Point(6, 18);
            this.Player_6_Name.Name = "Player_6_Name";
            this.Player_6_Name.Size = new System.Drawing.Size(156, 19);
            this.Player_6_Name.TabIndex = 1;
            this.Player_6_Name.TextChanged += new System.EventHandler(this.Player_6_Name_TextChanged);
            // 
            // Player_6_Count
            // 
            this.Player_6_Count.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Player_6_Count.Location = new System.Drawing.Point(6, 40);
            this.Player_6_Count.Name = "Player_6_Count";
            this.Player_6_Count.Size = new System.Drawing.Size(156, 23);
            this.Player_6_Count.TabIndex = 4;
            this.Player_6_Count.Text = "0";
            this.Player_6_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Player_6_Reset
            // 
            this.Player_6_Reset.Location = new System.Drawing.Point(6, 66);
            this.Player_6_Reset.Name = "Player_6_Reset";
            this.Player_6_Reset.Size = new System.Drawing.Size(75, 23);
            this.Player_6_Reset.TabIndex = 2;
            this.Player_6_Reset.Text = "リセット";
            this.Player_6_Reset.UseVisualStyleBackColor = true;
            this.Player_6_Reset.Click += new System.EventHandler(this.Player_6_Reset_Click);
            // 
            // Player_6_Save
            // 
            this.Player_6_Save.Location = new System.Drawing.Point(87, 66);
            this.Player_6_Save.Name = "Player_6_Save";
            this.Player_6_Save.Size = new System.Drawing.Size(75, 23);
            this.Player_6_Save.TabIndex = 3;
            this.Player_6_Save.Text = "保存";
            this.Player_6_Save.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.Player_7_Name);
            this.groupBox7.Controls.Add(this.Player_7_Count);
            this.groupBox7.Controls.Add(this.Player_7_Reset);
            this.groupBox7.Controls.Add(this.Player_7_Save);
            this.groupBox7.Location = new System.Drawing.Point(364, 332);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(169, 97);
            this.groupBox7.TabIndex = 8;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "7P";
            // 
            // Player_7_Name
            // 
            this.Player_7_Name.Location = new System.Drawing.Point(6, 18);
            this.Player_7_Name.Name = "Player_7_Name";
            this.Player_7_Name.Size = new System.Drawing.Size(156, 19);
            this.Player_7_Name.TabIndex = 1;
            this.Player_7_Name.TextChanged += new System.EventHandler(this.Player_7_Name_TextChanged);
            // 
            // Player_7_Count
            // 
            this.Player_7_Count.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Player_7_Count.Location = new System.Drawing.Point(6, 40);
            this.Player_7_Count.Name = "Player_7_Count";
            this.Player_7_Count.Size = new System.Drawing.Size(156, 23);
            this.Player_7_Count.TabIndex = 4;
            this.Player_7_Count.Text = "0";
            this.Player_7_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Player_7_Reset
            // 
            this.Player_7_Reset.Location = new System.Drawing.Point(6, 66);
            this.Player_7_Reset.Name = "Player_7_Reset";
            this.Player_7_Reset.Size = new System.Drawing.Size(75, 23);
            this.Player_7_Reset.TabIndex = 2;
            this.Player_7_Reset.Text = "リセット";
            this.Player_7_Reset.UseVisualStyleBackColor = true;
            this.Player_7_Reset.Click += new System.EventHandler(this.Player_7_Reset_Click);
            // 
            // Player_7_Save
            // 
            this.Player_7_Save.Location = new System.Drawing.Point(87, 66);
            this.Player_7_Save.Name = "Player_7_Save";
            this.Player_7_Save.Size = new System.Drawing.Size(75, 23);
            this.Player_7_Save.TabIndex = 3;
            this.Player_7_Save.Text = "保存";
            this.Player_7_Save.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.Player_8_Name);
            this.groupBox8.Controls.Add(this.Player_8_Count);
            this.groupBox8.Controls.Add(this.Player_8_Reset);
            this.groupBox8.Controls.Add(this.Player_8_Save);
            this.groupBox8.Location = new System.Drawing.Point(539, 332);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(169, 97);
            this.groupBox8.TabIndex = 9;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "8P";
            // 
            // Player_8_Name
            // 
            this.Player_8_Name.Location = new System.Drawing.Point(6, 18);
            this.Player_8_Name.Name = "Player_8_Name";
            this.Player_8_Name.Size = new System.Drawing.Size(156, 19);
            this.Player_8_Name.TabIndex = 1;
            this.Player_8_Name.TextChanged += new System.EventHandler(this.Player_8_Name_TextChanged);
            // 
            // Player_8_Count
            // 
            this.Player_8_Count.Font = new System.Drawing.Font("MS UI Gothic", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Player_8_Count.Location = new System.Drawing.Point(6, 40);
            this.Player_8_Count.Name = "Player_8_Count";
            this.Player_8_Count.Size = new System.Drawing.Size(156, 23);
            this.Player_8_Count.TabIndex = 4;
            this.Player_8_Count.Text = "0";
            this.Player_8_Count.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Player_8_Reset
            // 
            this.Player_8_Reset.Location = new System.Drawing.Point(6, 66);
            this.Player_8_Reset.Name = "Player_8_Reset";
            this.Player_8_Reset.Size = new System.Drawing.Size(75, 23);
            this.Player_8_Reset.TabIndex = 2;
            this.Player_8_Reset.Text = "リセット";
            this.Player_8_Reset.UseVisualStyleBackColor = true;
            this.Player_8_Reset.Click += new System.EventHandler(this.Player_8_Reset_Click);
            // 
            // Player_8_Save
            // 
            this.Player_8_Save.Location = new System.Drawing.Point(87, 66);
            this.Player_8_Save.Name = "Player_8_Save";
            this.Player_8_Save.Size = new System.Drawing.Size(75, 23);
            this.Player_8_Save.TabIndex = 3;
            this.Player_8_Save.Text = "保存";
            this.Player_8_Save.UseVisualStyleBackColor = true;
            // 
            // Count_All_Reset
            // 
            this.Count_All_Reset.Location = new System.Drawing.Point(364, 200);
            this.Count_All_Reset.Name = "Count_All_Reset";
            this.Count_All_Reset.Size = new System.Drawing.Size(344, 23);
            this.Count_All_Reset.TabIndex = 14;
            this.Count_All_Reset.Text = "オールリセット";
            this.Count_All_Reset.UseVisualStyleBackColor = true;
            this.Count_All_Reset.Click += new System.EventHandler(this.Count_All_Reset_Click);
            // 
            // Battle_Player
            // 
            this.Battle_Player.Location = new System.Drawing.Point(14, 202);
            this.Battle_Player.Name = "Battle_Player";
            this.Battle_Player.Size = new System.Drawing.Size(169, 19);
            this.Battle_Player.TabIndex = 15;
            this.Battle_Player.TextChanged += new System.EventHandler(this.Battle_Player_TextChanged);
            this.Battle_Player.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Battle_Player_KeyDown);
            // 
            // Battle_Player_Set
            // 
            this.Battle_Player_Set.Location = new System.Drawing.Point(189, 200);
            this.Battle_Player_Set.Name = "Battle_Player_Set";
            this.Battle_Player_Set.Size = new System.Drawing.Size(169, 23);
            this.Battle_Player_Set.TabIndex = 16;
            this.Battle_Player_Set.Text = "対戦開始";
            this.Battle_Player_Set.UseVisualStyleBackColor = true;
            this.Battle_Player_Set.Click += new System.EventHandler(this.Battle_Player_Set_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Items.AddRange(new object[] {
            "1P",
            "2P",
            "3P",
            "4P",
            "5P",
            "6P",
            "7P",
            "8P"});
            this.checkedListBox1.Location = new System.Drawing.Point(14, 80);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(169, 116);
            this.checkedListBox1.TabIndex = 17;
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "カウント対象外プレイヤー";
            // 
            // checkedListBox2
            // 
            this.checkedListBox2.FormattingEnabled = true;
            this.checkedListBox2.Items.AddRange(new object[] {
            "1P",
            "2P",
            "3P",
            "4P",
            "5P",
            "6P",
            "7P",
            "8P"});
            this.checkedListBox2.Location = new System.Drawing.Point(189, 80);
            this.checkedListBox2.Name = "checkedListBox2";
            this.checkedListBox2.Size = new System.Drawing.Size(169, 116);
            this.checkedListBox2.TabIndex = 19;
            this.checkedListBox2.SelectedIndexChanged += new System.EventHandler(this.checkedListBox2_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(187, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 12);
            this.label2.TabIndex = 20;
            this.label2.Text = "1P警告プレイヤー";
            // 
            // textBox1
            // 
            this.textBox1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.textBox1.Location = new System.Drawing.Point(107, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.ShortcutsEnabled = false;
            this.textBox1.Size = new System.Drawing.Size(76, 19);
            this.textBox1.TabIndex = 21;
            this.textBox1.Text = "3";
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 12);
            this.label3.TabIndex = 22;
            this.label3.Text = "処理開始対戦数";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(187, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 23;
            this.label4.Text = "総対戦数：0";
            // 
            // Chat_Link
            // 
            this.Chat_Link.Location = new System.Drawing.Point(364, 59);
            this.Chat_Link.Name = "Chat_Link";
            this.Chat_Link.Size = new System.Drawing.Size(344, 19);
            this.Chat_Link.TabIndex = 24;
            this.Chat_Link.Text = "https://studio.youtube.com/live_chat?is_popout=1&v=GjxWLLoM7Ck";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(362, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 12);
            this.label5.TabIndex = 25;
            this.label5.Text = "Chat Link";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(364, 84);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(344, 23);
            this.button2.TabIndex = 26;
            this.button2.Text = "コメント欄表示";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ファイルToolStripMenuItem,
            this.フルスクリーンToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1264, 24);
            this.menuStrip1.TabIndex = 27;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ファイルToolStripMenuItem
            // 
            this.ファイルToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.csvtxt出力ToolStripMenuItem,
            this.csv出力ToolStripMenuItem,
            this.txt出力ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.データロードToolStripMenuItem,
            this.toolStripMenuItem2,
            this.終了ToolStripMenuItem});
            this.ファイルToolStripMenuItem.Name = "ファイルToolStripMenuItem";
            this.ファイルToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ファイルToolStripMenuItem.Text = "ファイル";
            // 
            // csvtxt出力ToolStripMenuItem
            // 
            this.csvtxt出力ToolStripMenuItem.Name = "csvtxt出力ToolStripMenuItem";
            this.csvtxt出力ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.csvtxt出力ToolStripMenuItem.Text = ".csv/.txt出力";
            // 
            // csv出力ToolStripMenuItem
            // 
            this.csv出力ToolStripMenuItem.Name = "csv出力ToolStripMenuItem";
            this.csv出力ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.csv出力ToolStripMenuItem.Text = ".csv出力";
            this.csv出力ToolStripMenuItem.Click += new System.EventHandler(this.csv出力ToolStripMenuItem_Click);
            // 
            // txt出力ToolStripMenuItem
            // 
            this.txt出力ToolStripMenuItem.Name = "txt出力ToolStripMenuItem";
            this.txt出力ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.txt出力ToolStripMenuItem.Text = ".txt.出力";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(137, 6);
            // 
            // データロードToolStripMenuItem
            // 
            this.データロードToolStripMenuItem.Name = "データロードToolStripMenuItem";
            this.データロードToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.データロードToolStripMenuItem.Text = "データロード";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(137, 6);
            // 
            // 終了ToolStripMenuItem
            // 
            this.終了ToolStripMenuItem.Name = "終了ToolStripMenuItem";
            this.終了ToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.終了ToolStripMenuItem.Text = "終了";
            this.終了ToolStripMenuItem.Click += new System.EventHandler(this.終了ToolStripMenuItem_Click);
            // 
            // フルスクリーンToolStripMenuItem1
            // 
            this.フルスクリーンToolStripMenuItem1.Name = "フルスクリーンToolStripMenuItem1";
            this.フルスクリーンToolStripMenuItem1.Size = new System.Drawing.Size(80, 20);
            this.フルスクリーンToolStripMenuItem1.Text = "フルスクリーン";
            this.フルスクリーンToolStripMenuItem1.Click += new System.EventHandler(this.フルスクリーンToolStripMenuItem1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // webView21
            // 
            this.webView21.AllowExternalDrop = true;
            this.webView21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webView21.CreationProperties = null;
            this.webView21.DefaultBackgroundColor = System.Drawing.Color.White;
            this.webView21.Location = new System.Drawing.Point(714, 34);
            this.webView21.Name = "webView21";
            this.webView21.Size = new System.Drawing.Size(538, 852);
            this.webView21.Source = new System.Uri("https://www.youtube.com/live_chat?is_popout=1&v=hsJiYQyeE2g", System.UriKind.Absolute);
            this.webView21.TabIndex = 31;
            this.webView21.ZoomFactor = 1D;
            // 
            // MaxPlayer
            // 
            this.MaxPlayer.AutoSize = true;
            this.MaxPlayer.Location = new System.Drawing.Point(365, 114);
            this.MaxPlayer.Name = "MaxPlayer";
            this.MaxPlayer.Size = new System.Drawing.Size(76, 12);
            this.MaxPlayer.TabIndex = 32;
            this.MaxPlayer.Text = "最大プレイヤー";
            // 
            // maxplayer_name
            // 
            this.maxplayer_name.Location = new System.Drawing.Point(367, 130);
            this.maxplayer_name.Multiline = true;
            this.maxplayer_name.Name = "maxplayer_name";
            this.maxplayer_name.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.maxplayer_name.Size = new System.Drawing.Size(341, 66);
            this.maxplayer_name.TabIndex = 33;
            // 
            // webView22
            // 
            this.webView22.AllowExternalDrop = true;
            this.webView22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.webView22.CreationProperties = null;
            this.webView22.DefaultBackgroundColor = System.Drawing.Color.White;
            this.webView22.Location = new System.Drawing.Point(12, 490);
            this.webView22.Name = "webView22";
            this.webView22.Size = new System.Drawing.Size(696, 396);
            this.webView22.Source = new System.Uri("http://suwanohiro.html.xdomain.jp/tsacounter/write.html?display2", System.UriKind.Absolute);
            this.webView22.TabIndex = 34;
            this.webView22.ZoomFactor = 1D;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(281, 29);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 35;
            this.button1.Text = "リセット";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(488, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 12);
            this.label6.TabIndex = 36;
            this.label6.Text = ".csv出力";
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(543, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 12);
            this.label7.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(607, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 12);
            this.label8.TabIndex = 38;
            this.label8.Text = ".txt出力";
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(658, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 12);
            this.label9.TabIndex = 39;
            // 
            // outputFolder
            // 
            this.outputFolder.Location = new System.Drawing.Point(94, 438);
            this.outputFolder.Name = "outputFolder";
            this.outputFolder.Size = new System.Drawing.Size(452, 19);
            this.outputFolder.TabIndex = 40;
            this.outputFolder.Text = "\\\\192.168.0.10\\Nginx-Root\\2016konbu\\TSA_Counter\\ver2.0\\csv";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 441);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 12);
            this.label10.TabIndex = 41;
            this.label10.Text = "出力先フォルダ";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(633, 436);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 42;
            this.button3.Text = "保存";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // account
            // 
            this.account.Location = new System.Drawing.Point(94, 463);
            this.account.Name = "account";
            this.account.Size = new System.Drawing.Size(533, 19);
            this.account.TabIndex = 43;
            this.account.Text = "supermariomaker@outlook.jp";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 466);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(49, 12);
            this.label11.TabIndex = 44;
            this.label11.Text = "アカウント";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(633, 461);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 45;
            this.button4.Text = "保存";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(552, 436);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 46;
            this.button5.Text = "開く";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 898);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.account);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.outputFolder);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.webView22);
            this.Controls.Add(this.maxplayer_name);
            this.Controls.Add(this.MaxPlayer);
            this.Controls.Add(this.webView21);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Chat_Link);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkedListBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.Battle_Player_Set);
            this.Controls.Add(this.Battle_Player);
            this.Controls.Add(this.Count_All_Reset);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.webView21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.webView22)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox Player_1_Name;
        private System.Windows.Forms.Button Player_1_Reset;
        private System.Windows.Forms.Button Player_1_Save;
        private System.Windows.Forms.Label Player_1_Count;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox Player_2_Name;
        private System.Windows.Forms.Label Player_2_Count;
        private System.Windows.Forms.Button Player_2_Reset;
        private System.Windows.Forms.Button Player_2_Save;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox Player_3_Name;
        private System.Windows.Forms.Label Player_3_Count;
        private System.Windows.Forms.Button Player_3_Reset;
        private System.Windows.Forms.Button Player_3_Save;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox Player_4_Name;
        private System.Windows.Forms.Label Player_4_Count;
        private System.Windows.Forms.Button Player_4_Reset;
        private System.Windows.Forms.Button Player_4_Save;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox Player_5_Name;
        private System.Windows.Forms.Label Player_5_Count;
        private System.Windows.Forms.Button Player_5_Reset;
        private System.Windows.Forms.Button Player_5_Save;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox Player_6_Name;
        private System.Windows.Forms.Label Player_6_Count;
        private System.Windows.Forms.Button Player_6_Reset;
        private System.Windows.Forms.Button Player_6_Save;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox Player_7_Name;
        private System.Windows.Forms.Label Player_7_Count;
        private System.Windows.Forms.Button Player_7_Reset;
        private System.Windows.Forms.Button Player_7_Save;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox Player_8_Name;
        private System.Windows.Forms.Label Player_8_Count;
        private System.Windows.Forms.Button Player_8_Reset;
        private System.Windows.Forms.Button Player_8_Save;
        private System.Windows.Forms.Button Count_All_Reset;
        private System.Windows.Forms.TextBox Battle_Player;
        private System.Windows.Forms.Button Battle_Player_Set;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox checkedListBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox Chat_Link;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ファイルToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem csvtxt出力ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem csv出力ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem txt出力ToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem データロードToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 終了ToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private Microsoft.Web.WebView2.WinForms.WebView2 webView21;
        private System.Windows.Forms.Label MaxPlayer;
        private System.Windows.Forms.TextBox maxplayer_name;
        private System.Windows.Forms.ToolStripMenuItem フルスクリーンToolStripMenuItem1;
        private Microsoft.Web.WebView2.WinForms.WebView2 webView22;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox outputFolder;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox account;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
    }
}

