﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using System.Collections;
using System.IO;
using System.Security.AccessControl;

namespace TSA_Counter_Ver_2._0
{
    public partial class Form1 : Form
    {
        Display_Size ds = new Display_Size();
        int[] player_count = new int[8];
        string[] player_name = new string[8];
        bool[] not_count_player = new bool[8];
        bool[] alert_player = new bool[8];
        int all_battle_count = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //バックスペースが押された時は有効（Deleteキーも有効）
            if (e.KeyChar == '\b')
            {
                return;
            }

            //数値0～9以外が押された時はイベントをキャンセルする
            if ((e.KeyChar < '0' || '9' < e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for(int a = 0; a < 8; a++)
            {
                player_count[a] = 0;
                player_name[a] = "";
                not_count_player[a] = false;
                alert_player[a] = false;
            }
        }

        public string Live_Chat_ID()
        {
            string str = Chat_Link.Text;
            int num = str.IndexOf("v=") + 2;
            string result = str.Substring(num);
            int a = result.IndexOf("?");
            if (a >= 0)
            {
                result = result.Substring(0, a);
            }
            return result;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            /*
            YouTube_API api = new YouTube_API();
            api.Job(this);
            timer1.Start();
            */
            webView21.Source = new System.Uri(Chat_Link.Text);

            //現在フォームが存在しているディスプレイを取得
            System.Windows.Forms.Screen s = System.Windows.Forms.Screen.FromControl(this);
            //ディスプレイの高さと幅を取得
            int h = s.Bounds.Height;
            int w = s.Bounds.Width;

            /*
            this.Width = w;
            this.Height = h;
            */
            
        }

        public void Live_Comment(string[,] data) {
            /*
            int a = 0;
            int length = data.GetLength(0);
            LCD.Text = "";
            while(a < length)
            {
                if (a >= (length - 8))
                {
                    LCD.Text += data[a, 0];
                    LCD.Text += "\n" + data[a, 1];
                    if(a < length)
                    {
                        LCD.Text += "\n\n";
                    }
                }
                a++;
            }
            cm_cnt.Text = $"コメント数：{length}";
            */
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            YouTube_API api = new YouTube_API();
            api.Job(this);
        }

        private void コメ欄非表示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 737;
        }

        private void コメ欄表示ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Width = 1253;
        }

        private void 終了ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void oNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void oFFToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void フルスクリーンToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ds.Resize(this);
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            KeyCode kc = new KeyCode();
            this.Text = kc.Key(e);
        }

        private void フルスクリーンToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ds.Resize(this);
        }

        private void Player_1_Reset_Click(object sender, EventArgs e)
        {
            player_count[0] = 0;
            player_name[0] = "";
            update();
        }

        private void Player_2_Reset_Click(object sender, EventArgs e)
        {
            player_count[1] = 0;
            player_name[1] = "";
            update();
        }

        private void Player_3_Reset_Click(object sender, EventArgs e)
        {
            player_count[2] = 0;
            player_name[2] = "";
            update();
        }

        private void Player_4_Reset_Click(object sender, EventArgs e)
        {
            player_count[3] = 0;
            player_name[3] = "";
            update();
        }

        private void Player_5_Reset_Click(object sender, EventArgs e)
        {
            player_count[4] = 0;
            player_name[4] = "";
            update();
        }

        private void Player_6_Reset_Click(object sender, EventArgs e)
        {
            player_count[5] = 0;
            player_name[5] = "";
            update();
        }

        private void Player_7_Reset_Click(object sender, EventArgs e)
        {
            player_count[6] = 0;
            player_name[6] = "";
            update();
        }

        private void Player_8_Reset_Click(object sender, EventArgs e)
        {
            player_count[7] = 0;
            player_name[7] = "";
            update();
        }

        private void Battle_Player_Set_Click(object sender, EventArgs e)
        {
            BPSC();
        }

        private void BPSC()
        {
            update();
            string BP = Battle_Player.Text;
            int bp_int;
            if (int.TryParse(BP, out bp_int) && BP.Length <= 4 && BP.Length > 0)
            {
                int[] bps = new int[0];
                for (int a = 0; a < BP.Length; a++)
                {
                    Array.Resize(ref bps, (bps.Length + 1));
                    bps[a] = bp_int % 10;
                    bp_int /= 10;
                }
                Array.Reverse(bps);

                for (int a = 0; a < bps.Length; a++)
                {
                    int num = (bps[a] - 1);
                    if (player_name[num] != "" && !not_count_player[num]) player_count[num]++;
                }

                if (alert_player[bps[0] - 1]) MessageBox.Show("1P確認", "警告", MessageBoxButtons.OK, MessageBoxIcon.Error);

                all_battle_count++;
                update();
            }
            else
            {
                MessageBox.Show("エラー", "エラー", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Battle_Player.Text = "";
        }

        private void update()
        {
            Player_1_Count.Text = player_count[0].ToString();
            Player_2_Count.Text = player_count[1].ToString();
            Player_3_Count.Text = player_count[2].ToString();
            Player_4_Count.Text = player_count[3].ToString();
            Player_5_Count.Text = player_count[4].ToString();
            Player_6_Count.Text = player_count[5].ToString();
            Player_7_Count.Text = player_count[6].ToString();
            Player_8_Count.Text = player_count[7].ToString();

            Player_1_Name.Text = player_name[0];
            Player_2_Name.Text = player_name[1];
            Player_3_Name.Text = player_name[2];
            Player_4_Name.Text = player_name[3];
            Player_5_Name.Text = player_name[4];
            Player_6_Name.Text = player_name[5];
            Player_7_Name.Text = player_name[6];
            Player_8_Name.Text = player_name[7];

            for (int i = 0; i < 8; i++)
            {
                if (checkedListBox1.GetItemCheckState(i) == CheckState.Checked)
                {
                    not_count_player[i] = true;
                }
                else
                {
                    not_count_player[i] = false;
                }
                if (checkedListBox2.GetItemCheckState(i) == CheckState.Checked)
                {
                    alert_player[i] = true;
                }
                else
                {
                    alert_player[i] = false;
                }
            }

            int max = player_count.Max();
            string str = "";
            for (int a = 0; a < player_count.Length; a++)
            {
                if (player_count[a] == max && max > 0)
                {
                    str += $"{player_name[a]}\r\n";
                }
            }
            maxplayer_name.Text = str;
            label4.Text = $"総対戦数：{all_battle_count}";
        }

        private void Player_1_Name_TextChanged(object sender, EventArgs e)
        {
            player_name[0] = Player_1_Name.Text;
        }

        private void Player_2_Name_TextChanged(object sender, EventArgs e)
        {
            player_name[1] = Player_2_Name.Text;
        }

        private void Player_3_Name_TextChanged(object sender, EventArgs e)
        {
            player_name[2] = Player_3_Name.Text;
        }

        private void Player_4_Name_TextChanged(object sender, EventArgs e)
        {
            player_name[3] = Player_4_Name.Text;
        }

        private void Player_5_Name_TextChanged(object sender, EventArgs e)
        {
            player_name[4] = Player_5_Name.Text;
        }

        private void Player_6_Name_TextChanged(object sender, EventArgs e)
        {
            player_name[5] = Player_6_Name.Text;
        }

        private void Player_7_Name_TextChanged(object sender, EventArgs e)
        {
            player_name[6] = Player_7_Name.Text;
        }

        private void Player_8_Name_TextChanged(object sender, EventArgs e)
        {
            player_name[7] = Player_8_Name.Text;
        }

        private void Count_All_Reset_Click(object sender, EventArgs e)
        {
            for(int a = 0; a < 8; a++)
            {
                player_count[a] = 0;
                player_name[a] = "";
            }

            update();
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void checkedListBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void Battle_Player_TextChanged(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            all_battle_count = 0;
            update();
        }

        private void csv出力ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(outputFolder.Text != "")
            {
                string textfile = outputFolder.Text;
                string write_data;

                if (!System.IO.File.Exists(textfile))
                {
                    Directory.CreateDirectory(textfile);
                    using (FileStream fs = File.Create(textfile + "\\data.csv"));
                }
                textfile += "\\data.csv";
                AddFileSecurity(textfile, account.Text, FileSystemRights.FullControl, AccessControlType.Allow);


                text txt = new text();

                txt.ClearTextFile(textfile);

                // 文字コードを指定
                Encoding enc = Encoding.GetEncoding("UTF-8");

                // ファイルを開く
                StreamWriter writer = new StreamWriter(@textfile, true, enc);

                // テキストを書き込む

                write_data = Csv_Maker();
                MessageBox.Show(Csv_Maker());

                //改行なし
                writer.Write(write_data);

                // ファイルを閉じる
                writer.Close();
            }
        }

        public static void AddFileSecurity(string fileName, string account, FileSystemRights rights, AccessControlType controlType)
        {
            // Get a FileSecurity object that represents the
            // current security settings.
            FileSecurity fSecurity = File.GetAccessControl(fileName);

            // Add the FileSystemAccessRule to the security settings.
            fSecurity.AddAccessRule(new FileSystemAccessRule(account, rights, controlType));

            // Set the new access settings.
            File.SetAccessControl(fileName, fSecurity);
        }

        private string Csv_Maker()
        {
            string result = "";
            for(int a = 0; a < player_count.Length; a++)
            {
                if (player_name[a] != "")
                {
                    result += $"{player_name[a]},{player_count[a]}";
                }
                else
                {
                    result += ",";
                }
                if (a < player_count.Length - 1) result += "\r\n";
            }
            return result;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@outputFolder.Text);
        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void Battle_Player_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                BPSC();
            }
        }
    }

    class YouTube_API
    {
        public void Job(Form1 frm)
        {
            var asd = Main(frm);
        }

        static async Task Main(Form1 frm)
        {
            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                //APIキー
                ApiKey = "AIzaSyAKAfnaHBTvBQM68Fx8iUOJTe1dTlHD2bo"
            });

            string liveChatId = GetliveChatID(frm.Live_Chat_ID(), youtubeService);

            await GetLiveChatMessage(liveChatId, youtubeService, null, frm);

        }

        static public string GetliveChatID(string videoId, YouTubeService youtubeService)
        {
            //引数で取得したい情報を指定
            var videosList = youtubeService.Videos.List("LiveStreamingDetails");
            videosList.Id = videoId;
            //動画情報の取得
            var videoListResponse = videosList.Execute();
            //LiveChatIDを返す
            foreach (var videoID in videoListResponse.Items)
            {
                return videoID.LiveStreamingDetails.ActiveLiveChatId;
            }
            //動画情報取得できない場合はnullを返す
            return null;
        }

        static public async Task GetLiveChatMessage(string liveChatId, YouTubeService youtubeService, string nextPageToken, Form1 frm)
        {
            var liveChatRequest = youtubeService.LiveChatMessages.List(liveChatId, "snippet,authorDetails");
            liveChatRequest.PageToken = nextPageToken;

            var liveChatResponse = await liveChatRequest.ExecuteAsync();

            string[] un = new string[0];
            string[] cm = new string[0];
            int cnt = 0;

            foreach (var liveChat in liveChatResponse.Items)
            {
                try
                {
                    string User_Name = liveChat.AuthorDetails.DisplayName;
                    string comment = liveChat.Snippet.DisplayMessage;
                    Array.Resize(ref un, un.Length + 1);
                    Array.Resize(ref cm, cm.Length + 1);
                    un[cnt] = User_Name;
                    cm[cnt] = comment;
                    //MessageBox.Show($"{un[cnt]}\n{cm[cnt]}");
                    cnt++;
                }
                catch { }

            }

            int a = 0;
            string[,] data = new string[un.Length, 2];
            while(a < un.Length)
            {
                data[a, 0] = un[a];
                data[a, 1] = cm[a];
                a++;
            }
            frm.Live_Comment(data);

            //自動的に新しいコメントを読む
            bool Auto_Read_New_Comment = false;
            if (Auto_Read_New_Comment)
            {
                await Task.Delay((int)liveChatResponse.PollingIntervalMillis);


                await GetLiveChatMessage(liveChatId, youtubeService, liveChatResponse.NextPageToken, frm);
            }
        }
    }

    class Display_Size
    {
        private FormWindowState prevFormState;
        private FormBorderStyle prevFormStyle;
        private Size prevFormSize;
        private bool display = false;
        private bool First_time = true;

        public void Resize(Form1 frm)
        {
            if (display)
            {
                display = false;
                Minimize(frm);
            }
            else
            {
                if (First_time)
                {
                    prevFormState = FormWindowState.Normal;
                    prevFormStyle = FormBorderStyle.Sizable;
                    prevFormSize = new Size(frm.Width, frm.Height);
                    First_time = false;
                }

                display = true;
                Maximize(frm);
            }
        }

        private void Maximize(Form1 frm)
        {
            prevFormState = frm.WindowState;
            prevFormStyle = frm.FormBorderStyle;

            if (frm.WindowState == FormWindowState.Maximized)
            {
                frm.WindowState = FormWindowState.Normal;
            }

            prevFormSize = frm.ClientSize;

            frm.FormBorderStyle = FormBorderStyle.None;
            frm.WindowState = FormWindowState.Maximized;
        }

        private void Minimize(Form1 frm)
        {
            frm.ClientSize = prevFormSize;

            if (prevFormState == FormWindowState.Maximized)
            {
                frm.WindowState = FormWindowState.Normal;
            }

            frm.FormBorderStyle = prevFormStyle;
            frm.WindowState = prevFormState;
        }
    }

    class KeyCode
    {
        private string Save_Key = "";

        public string Key(KeyEventArgs e) {
            KeysConverter kc = new KeysConverter();
            return kc.ConvertToString(e.KeyCode);
        }

        public void Key_Save(KeyEventArgs e)
        {
            KeysConverter kc = new KeysConverter();
            Save_Key = kc.ConvertToString(e.KeyCode);
        }

        public string Key_Load()
        {
            return Save_Key;
        }
    }

    class text
    {
        public void ClearTextFile(string filePathName)
        {
            using (var fileStream = new FileStream(filePathName, FileMode.Open))
            {
                // ストリームの長さを0に設定します。
                // 結果としてファイルのサイズが0になります。
                fileStream.SetLength(0);
            }
        }
    }

}
